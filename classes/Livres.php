<?php
class Livres{
// attributs
    private $_id,
    $_titre,
    $_date_publi, 
    $_resume, 
    $_auteur,
    $_genre,
    $_stock;


// constructor
   public function __construct(Array $info){

// hydrate les objets
    $this->hydrate($info);


    }
    private function hydrate(Array $info){
        foreach ($param as $key=>$value){
            $methode = "set".ucfirst($key);
            if (method_exists($this, $methode)){
                $this->$methode.value;
            } 
        

    }
    
}
    public function getId(){
        return this->_id;
    }
    public function getTitre(){
        return this->_titre;
    }
    public function getDatepubli(){
        return this->_Datepubli;
    }
    public function getResume(){
        return this->_Resume;
    }
    public function getAuteur(){
        return this->_auteur;
    }
    public function getGenre(){
        return this->_genre;
    }
    public function getStock(){
        return this->_stock;
    }

    private function setId(int $id){
        $this->_id = $id;
    }
    
    private function setTitre( string $titre){
        $titre = htmlsepcialchars($titre);
        $this-> _titre = $titre;

    }
    private function setDate_publi($date_publi){
        $date_publi = date('d/m/y', $date_publi);
        $this-> _date_publi =  $date_publi;
       

    }
    private function setAuteur(string $auteur){
        
       
        $this-> _auteur = $auteur;

    }
    private function setGenre(string $genre){
        $this-> _genre = $genre;

    }
 
    private function setStock(int $stock){
        if ($stock <0){
            $stock = 0;
        }else{
        $this-> _stock = $stock;
    }

    }
 

    }
    