#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateurs
#------------------------------------------------------------

CREATE TABLE Utilisateurs(
        id           Int  Auto_increment  NOT NULL ,
        nom          Varchar (50) NOT NULL ,
        prenom       Varchar (50) NOT NULL ,
        email        Varchar (50) NOT NULL ,
        password     Varchar (50) NOT NULL ,
        telephone    Varchar (20) NOT NULL ,
        login        Varchar (50) NOT NULL ,
        gestionnaire Bool  NULL
	,CONSTRAINT Utilisateurs_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Auteurs
#------------------------------------------------------------

CREATE TABLE Auteurs(
        id       Int  Auto_increment  NOT NULL ,
        nom      Varchar (50) NOT NULL ,
        prenom   Varchar (50) NOT NULL ,
        date_vie Varchar (50) NOT NULL
	,CONSTRAINT Auteurs_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Genre
#------------------------------------------------------------

CREATE TABLE Genre(
        id     Int  Auto_increment  NOT NULL ,
        nom    Varchar (50) NOT NULL ,
        resume Mediumtext NOT NULL
	,CONSTRAINT Genre_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Livres
#------------------------------------------------------------

CREATE TABLE Livres(
        id         Int  Auto_increment  NOT NULL ,
        titre      Varchar (50) NOT NULL ,
        date_publi Date NOT NULL ,
        resume     Mediumtext NOT NULL ,
        stock      Int NOT NULL
	,CONSTRAINT Livres_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: RelationLivresAuteurs
#------------------------------------------------------------

CREATE TABLE RelationLivresAuteurs(
        id_livres        Int NOT NULL ,
        id_Auteurs Int NOT NULL
	,CONSTRAINT RelationLivresAuteurs_PK PRIMARY KEY (id_livres,id_Auteurs)

	,CONSTRAINT RelationLivresAuteurs_Livres_FK FOREIGN KEY (id_livres) REFERENCES Livres(id)
	,CONSTRAINT RelationLivresAuteurs_Auteurs0_FK FOREIGN KEY (id_Auteurs) REFERENCES Auteurs(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: RelationLivresGenres
#------------------------------------------------------------

CREATE TABLE RelationLivresGenres(
        id_livres      Int NOT NULL ,
        id_Genre Int NOT NULL
	,CONSTRAINT RelationLivresGenres_PK PRIMARY KEY (id_livres,id_Genre)

	,CONSTRAINT RelationLivresGenres_Livres_FK FOREIGN KEY (id_livres) REFERENCES Livres(id)
	,CONSTRAINT RelationLivresGenres_Genre0_FK FOREIGN KEY (id_Genre) REFERENCES Genre(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: emprunts
#------------------------------------------------------------

CREATE TABLE emprunts(
        id_livres            Int NOT NULL ,
        id_Utilisateurs Int NOT NULL ,
        date_emprun    Date NOT NULL ,
        date_retour    Date NOT NULL
	,CONSTRAINT emprunts_PK PRIMARY KEY (id_livres ,id_Utilisateurs)

	,CONSTRAINT emprunts_Livres_FK FOREIGN KEY (id_livres ) REFERENCES Livres(id)
	,CONSTRAINT emprunts_Utilisateurs0_FK FOREIGN KEY (id_Utilisateurs) REFERENCES Utilisateurs(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: reservations
#------------------------------------------------------------

CREATE TABLE reservations(
        id_livres            Int NOT NULL ,
        id_Utilisateurs Int NOT NULL ,
        date_mise_cote Date NOT NULL
	,CONSTRAINT reservations_PK PRIMARY KEY (id_livres,id_Utilisateurs)

	,CONSTRAINT reservations_Livres_FK FOREIGN KEY (id_livres) REFERENCES Livres(id)
	,CONSTRAINT reservations_Utilisateurs0_FK FOREIGN KEY (id_Utilisateurs) REFERENCES Utilisateurs(id)
)ENGINE=InnoDB;

